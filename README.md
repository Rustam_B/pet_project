# Pet_project



Учебный проект в рамках изучения Django framework

Описание: простой issue tracker

Для ознакомления:
 - склонировать удаленный репозиторий
 - pip install requirements.txt
 - python3 manage.py migrate

Используемые технологии:

    MTV

Модели стандартные
Для форм - модельные формы
Шаблонизатор HTML, оформление static CSS
Для представлений использовались Class based views
Авторизация, аутотенфикация
