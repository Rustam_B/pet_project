from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model

class Type(models.Model):
    name = models.TextField(max_length=100, null=False, blank=False, verbose_name='Тип задачи')

    def __str__(self):
        return self.name

class Status(models.Model):
    name = models.TextField(max_length=100, null=False, blank=False, verbose_name='Статус задачи')

    def __str__(self):
        return self.name

class Project(models.Model):
    date_start = models.DateField(verbose_name='Дата начала', null=False, blank=False)
    date_end = models.DateField(verbose_name='Дата окончания', blank=True, null=True)
    project_name = models.CharField(max_length=250, null=False, blank=False, verbose_name='Наименование проекта')
    project_description = models.TextField(max_length=1000, null=True, verbose_name='Описание проекта')
    users = models.ManyToManyField(User, related_name='users', blank=True, verbose_name='Пользователи')
    author = models.ForeignKey(get_user_model(), on_delete=models.SET_DEFAULT, default=1, verbose_name='Автор')

    def __str__(self):
        return self.project_name

    class Meta:
        permissions = [
            ('can_add_user_to_project', 'Can add user to project')
        ]

class Task(models.Model):
    summary = models.CharField(max_length=250, null=False, blank=False, verbose_name='Заголовок')
    description = models.TextField(max_length=1000, null=True, verbose_name='Описание')
    status = models.ForeignKey(Status, related_name='Tasks', on_delete=models.PROTECT, verbose_name='Статус')
    type = models.ManyToManyField(Type, related_name='type', blank=True, verbose_name='Тип задачи')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Время создания')
    update_at = models.DateTimeField(auto_now=True, verbose_name='Время изменения')
    project = models.ForeignKey(Project, related_name='tasks', default=None, on_delete=models.CASCADE, verbose_name='Проект')

    def __str__(self):
        return self.summary