from django.contrib import admin
from Tracker.models import Type, Status, Task, Project

admin.site.register(Type)
admin.site.register(Status)
admin.site.register(Task)
admin.site.register(Project)