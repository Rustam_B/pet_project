from django.shortcuts import get_object_or_404, redirect
from Tracker.models import *
from django.views.generic import TemplateView, FormView, ListView, DetailView, CreateView, UpdateView
from django.urls import reverse, reverse_lazy
from .forms import AddForm, SearchForm, AddFormProject
from django.utils.html import urlencode
from django.db.models import Q
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin, UserPassesTestMixin

class IndexView(ListView):#вывод всех задач на главной
    template_name = 'index.html'
    model = Task
    context_object_name = 'all'
    paginate_by = 3

    def get(self, request, *args, **kwargs):
        self.form = self.get_search_form()
        self.search_value = self.get_search_value()
        return super().get(request, *args, **kwargs)

    def get_context_data(self, *, object_list=None, **kwargs):
        contex = super().get_context_data(object_list=object_list, **kwargs)
        contex['form'] = self.form
        if self.search_value:
            contex['query'] = urlencode({'search': self.search_value})
        return contex

    def get_queryset(self):
        queryset = super().get_queryset()
        if self.search_value:
            query = Q(summary__icontains=self.search_value) | Q(description__icontains=self.search_value)
            queryset = queryset.filter(query)
        return queryset

    def get_search_form(self):
        return SearchForm(self.request.GET)

    def get_search_value(self):
        if self.form.is_valid():
            return self.form.cleaned_data['search']

class TaskDetailView(TemplateView):#просмотр детальной информации о задаче
    template_name = 'Task/task_info.html'

    def get_context_data(self, **kwargs):
        contex = super().get_context_data(**kwargs)
        contex['all'] = get_object_or_404(Task, pk=kwargs['pk'])
        return contex

class TaskDeleteView(LoginRequiredMixin,TemplateView):#Удаление задачи без подтверждения
    template_name = 'index.html'

    def post(self, request, pk):
        item = get_object_or_404(Task, pk=pk)
        item.delete()
        return redirect(reverse('home'))

class TaskAddView(PermissionRequiredMixin, CreateView):#Добавление задачи
    template_name = 'Task/task_add.html'
    model = Task
    form_class = AddForm
    permission_required = 'Tracker.add_task'

    def get_success_url(self):
        return reverse('home')


class TaskEditView(LoginRequiredMixin, FormView):#Редактирование задачи
    template_name = 'Task/task_edit.html'
    form_class = AddForm
    permission_required = 'Tracker.change_task'

    def dispatch(self, request, *args, **kwargs):
        self.obj = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def get_object(self):
        pk = self.kwargs.get('pk')
        return get_object_or_404(Task, pk=pk)

    def get_context_data(self, **kwargs):
        contex = super().get_context_data(**kwargs)
        contex['obj'] = self.obj
        return contex

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['instance'] = self.obj
        return kwargs

    def form_valid(self, form):
        self.obj = form.save()
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('home')

class ProjectView(ListView):#вывод всех проектов на странице
    template_name = 'Project/projects.html'
    model = Project
    context_object_name = 'project'

class ProjectDetailView(LoginRequiredMixin, DetailView):#просмотр детальной иформации о проекте
    template_name = 'Project/project_info.html'
    model = Project

    def test_func(self):
        return self.request.user.has_perm('Tracker.change_task')

class ProjectAddView(PermissionRequiredMixin, CreateView):#создание проекта
    template_name = 'Project/project_add.html'
    model = Project
    form_class = AddFormProject
    permission_required = 'Tracker.add_project'

    def get_success_url(self):
        return reverse('project')

class AddUserProject(PermissionRequiredMixin, UpdateView):#Добавить Удалить пользователя из проекта
    model = Project
    template_name = 'Project/add_user_project.html'
    fields = ['users']
    permission_required = 'Tracker.can_add_user_to_project'

    def get_success_url(self):
        return reverse('project_info', kwargs={'pk': self.get_object().pk})



