from rest_framework import generics, status
from rest_framework.response import Response

from Tracker.serializers import TaskSerializer, ProjectSerializer
from Tracker.models import Task, Project

class IndexAPIView(generics.ListCreateAPIView): # вывод всех задач + создание задачи
    queryset = Task.objects.all()
    serializer_class = TaskSerializer
    print(serializer_class)

class ProjectAPIView(generics.ListCreateAPIView): # вывод всех проектов + создание проекта
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer

    def perform_create(self, serializer):
        serializer.save(type=self.request.q)

    # def post(self, request, *args, **kwargs):
    #     serializer = ProjectSerializer(data=request.data)
    #     if serializer.is_valid(raise_exception=True):
    #         serializer.save()
    #         return Response(serializer.data, status=status.HTTP_201_CREATED)
    #     return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



class TaskDetailAPIView(generics.RetrieveAPIView): # вывод подробного описания о задаче
    queryset = Task.objects.all()
    serializer_class = TaskSerializer

class ProjectDetailAPIView(generics.RetrieveAPIView): # вывод подробного описания о проекте
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer


