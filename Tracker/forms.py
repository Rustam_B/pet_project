from django import forms
from .models import *

class AddFormProject(forms.ModelForm):#форма для проекта
    class Meta:
        exclude = ['author']
        model = Project
        widgets = {
            'date_start': forms.DateInput(format=('%m/%d/%Y'), attrs={'class': 'form-control', 'type': 'date'}),
            'date_end': forms.DateInput(format=('%m/%d/%Y'), attrs={'class': 'form-control', 'type': 'date'}),
            'project_name': forms.TextInput(attrs={'size': '50'})
        }


class AddForm(forms.ModelForm):#форма для задачи
    class Meta:
        fields = '__all__'
        model = Task
        widgets = {
            'type': forms.CheckboxSelectMultiple,
        }

    def clean(self):
        super().clean()

        summary = self.cleaned_data['summary']
        description = self.cleaned_data['description']
        if len(summary) < 10:
            self._errors['summary'] = self.error_class([
                'Минимум 10 символов в задаче'])
        if len(description) < 15:
            self._errors['description'] = self.error_class([
                'Минимум 15 символов в описании'])
        return self.cleaned_data

class SearchForm(forms.Form):
    search = forms.CharField(required=False, label='Поиск', widget=forms.TextInput(attrs={'placeholder': 'Задача или описание'}))