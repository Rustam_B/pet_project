from rest_framework import serializers
from Tracker.models import Task, Project, Type

class TypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Type
        fields = ('name',)

class TaskSerializer(serializers.ModelSerializer):
    type = TypeSerializer(many=True, read_only=True).data
    class Meta:
        model = Task
        fields = ('summary', 'description', 'status', 'type', 'project')

    def create(self, validated_data):
        return Task.objects.create(**validated_data)

class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = ('date_start', 'date_end', 'project_name', 'project_description', 'author')

    def create(self, validated_data):
        return Project.objects.create(**validated_data)


