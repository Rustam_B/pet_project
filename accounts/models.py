from django.db import models
from django.contrib.auth import get_user_model

class Profile(models.Model):
    user = models.OneToOneField(get_user_model(), related_name='profile', on_delete=models.CASCADE, verbose_name='ПОЛЬзователь')
    avatar = models.ImageField(null=True, blank=True, upload_to='avatars', verbose_name='Аватарка')
    website= models.URLField(max_length=250, verbose_name='Ссылка на профиль', null=True, blank=True)
    description = models.TextField(max_length=1000, null=True, blank=True, verbose_name='О себе')