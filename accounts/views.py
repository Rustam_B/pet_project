from django.shortcuts import redirect, render, reverse
from django.views.generic import CreateView, DetailView, ListView, UpdateView
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model, update_session_auth_hash
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin

from django.contrib.auth import login, logout
from accounts.models import Profile
from accounts.forms import UserLoginForm, UserRegistrationForm, UserChangeForm, ProfileChangeForm, PasswordChangeForm


def user_login(request):#функция логирования клиента
    if request.method == 'POST':
        form = UserLoginForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            return redirect('home')
    else:
        form = UserLoginForm()
    return render(request, 'login.html', {'form': form})

def user_logout(request):#функция выхода клиента
    logout(request)
    return redirect('login')

class UserRegisterView(CreateView):#регистрация клиента
    model = User
    template_name = 'registration.html'
    form_class = UserRegistrationForm

    def form_valid(self, form):
        user = form.save()
        Profile.objects.create(user=user)
        login(self.request, user)
        return redirect('home')

    def get_success_url(self):
        next_url = self.request.GET.get('next')
        if not next_url:
            next_url = self.request.POST.get('next')
        if not next_url:
            next_url = reverse('home')
        return next_url

class UserDetailView(LoginRequiredMixin, DetailView):#Профиль пользователя
    model = get_user_model()
    template_name = 'User/user_detail.html'
    context_object_name = 'user_obj'

    def get_context_data(self, **kwargs):
        projects = self.object.users
        kwargs['projects'] = projects
        return super().get_context_data(**kwargs)

class UsersListView(PermissionRequiredMixin, ListView):#вывод всех пользователей
    template_name = 'User/users_all.html'
    model = User
    context_object_name = 'users_all'
    permission_required = 'auth.view_user'

class UserChangeView(UpdateView):#редактирование пользовательских данных
    model = get_user_model()
    form_class = UserChangeForm
    template_name = "User/edit_profile.html"
    context_object_name = "user_obj"

    def get_profile_form(self):
        form_kwargs = {"instance": self.object.profile}
        if self.request.method == "POST":
            form_kwargs["data"] = self.request.POST
            form_kwargs["files"] = self.request.FILES
        return ProfileChangeForm(**form_kwargs)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        user_form = self.get_form()
        profile_form = self.get_profile_form()
        if user_form.is_valid() and profile_form.is_valid():
            return self.form_valid(user_form, profile_form)
        else:
            return self.form_invalid(user_form, profile_form)

    def form_valid(self, user_form, profile_form):
        result = super().form_valid(user_form)
        profile_form.save()
        return result

    def form_invalid(self, user_form, profile_form):
        context = self.get_context_data(
            user_form=user_form,
            profile_form=profile_form
        )
        return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        if "profile_form" not in kwargs:
            kwargs["profile_form"] = self.get_profile_form()
            kwargs["user_form"] = self.get_form()
        return super().get_context_data(**kwargs)

    def get_success_url(self):
        return reverse("user_detail", kwargs={"pk": self.object.pk})

class ChangePasswordView(LoginRequiredMixin, UpdateView):
    model = get_user_model()
    form_class = PasswordChangeForm
    template_name = 'User/user_change_password.html'
    context_object_name = "user_obj"

    def form_valid(self, form):
        form.save()
        update_session_auth_hash(self.request, form.instance)
        return redirect('user_detail', pk=self.get_object().pk)
