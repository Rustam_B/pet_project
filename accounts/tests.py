from django.test import TestCase
from django.contrib.auth import get_user_model
from django.contrib.auth import authenticate
from django.shortcuts import reverse

# class AuthorizationUser(TestCase):
#     def setUp(self):
#         self.user = get_user_model().objects.create_user(username='test', password='12test12')
#         self.user.save()
#
#     def tearDown(self):
#         self.user.delete()
#
#     def test_correct(self):
#         user = authenticate(username='test', password='12test12')
#         self.assertTrue((user is not None) and user.is_authenticated)
#
#     def test_wrong_username(self):
#         user = authenticate(username='wrong', password='12test12')
#         self.assertFalse(user is not None and user.is_authenticated)
#
#     def test_wrong_password(self):
#         user = authenticate(username='test', password='wrong')
#         self.assertFalse(user is not None and user.is_authenticated)


# class SignUpPageTests(TestCase):
#     def setUp(self) -> None:
#         self.username = 'testuser'
#         self.email = 'testuser@email.com'
#         self.password = 'password'
#         self.first_name = 'Ivan'
#         self.last_name = 'Petrov'
#
#     def test_signup_page_url(self):
#         response = self.client.get("/registration/")
#         self.assertEqual(response.status_code, 200)
#         self.assertTemplateUsed(response, template_name='registration.html')
#
#     def test_signup_page_view_name(self):
#         response = self.client.get(reverse('registration'))
#         self.assertEqual(response.status_code, 200)
#         self.assertTemplateUsed(response, template_name='registration.html')
#
#     def test_signup_form(self):
#         response = self.client.post(reverse('registration'), data={
#             'username': self.username,
#             'email': self.email,
#             'first_name': self.first_name,
#             'last_name': self.last_name,
#             'password1': self.password,
#             'password2': self.password
#         })
#
#         self.assertEqual(response.status_code, 200)
#         users = get_user_model().objects.all()
#         self.assertEqual(users.count(), 1)