from django.contrib.auth.forms import AuthenticationForm, UsernameField, UserCreationForm
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model

from accounts.models import Profile

class UserLoginForm(AuthenticationForm):#форма для авторизации
    error_messages = {'invalid_login': 'Неверный логин или пароль'}

    username = forms.CharField(label='Имя пользователя', widget=forms.TextInput(attrs={'class': 'form-control'}),
                               error_messages={'required': error_messages.get('required_username')})
    password = forms.CharField(label='Пароль', widget=forms.PasswordInput(attrs={'class': 'form-control'}),
                               error_messages={'required': error_messages.get('required_password')})

class UserRegistrationForm(UserCreationForm):#форма для регистрации
    username = forms.CharField(label='Логин', widget=forms.TextInput(attrs={'class': 'form-control'}))
    first_name = forms.CharField(label='Имя пользователя', widget=forms.TextInput(attrs={'class': 'form-control'}))
    last_name = forms.CharField(label='Фамилия пользователя', widget=forms.TextInput(attrs={'class': 'form-control'}))
    email = forms.EmailField(label='E-mail', widget=forms.EmailInput(attrs={'class': 'form-control'}))
    password1 = forms.CharField(label='Пароль', widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    password2 = forms.CharField(label='Подтверждение пароля', widget=forms.PasswordInput(attrs={'class': 'form-control'}))

    field_order = ['username', 'first_name', 'last_name', 'email', 'password1']

    class Meta:
        model = User
        fields = {'username', 'first_name', 'last_name', 'email', 'password1', 'password2'}

class UserChangeForm(forms.ModelForm):
    class Meta:
        model = get_user_model()
        fields = ["first_name", "last_name", "email"]
        labels = {"first_name": 'Имя', "last_name": 'Фамилия', "email": "Email"}

class ProfileChangeForm(forms.ModelForm):
    class Meta:
        model = Profile
        exclude = ["user"]

        widgets = {
            'website': forms.TextInput(attrs={'size': '50'})
        }

class PasswordChangeForm(forms.ModelForm):#форма смены пароля
    old_password = forms.CharField(label="Старый пароль", strip=False,
                                   widget=forms.PasswordInput)
    password = forms.CharField(label="Новый пароль", strip=False,
                               widget=forms.PasswordInput)
    password_confirm = forms.CharField(label="Подтверждение", strip=False,
                                       widget=forms.PasswordInput)

    def clean_password_confirm(self):
        password = self.cleaned_data["password"]
        password_confirm = self.cleaned_data["password_confirm"]
        if password and password_confirm and password != password_confirm:
            raise forms.ValidationError("Пароли не совпадают")
        return password_confirm

    def clean_old_password(self):
        old_password = self.cleaned_data["old_password"]
        if not self.instance.check_password(old_password):
            raise forms.ValidationError("Wrong old password")
        return old_password

    def save(self, commit=True):
        user = self.instance
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()

        return user

    class Meta:
        model = get_user_model()
        fields = ["old_password", "password", "password_confirm"]


