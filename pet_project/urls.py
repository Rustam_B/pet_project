from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from Tracker.views import *
from Tracker.apiviews import IndexAPIView, ProjectAPIView, TaskDetailAPIView, ProjectDetailAPIView
from accounts.views import user_login, \
    user_logout, \
    UserRegisterView, \
    UserDetailView, \
    UsersListView, \
    UserChangeView, \
    ChangePasswordView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', IndexView.as_view(), name='home'),
    path('task_info/<int:pk>/', TaskDetailView.as_view(), name='task_info'),
    path('task_del/<int:pk>/', TaskDeleteView.as_view(), name='task_delete'),
    path('task_add', TaskAddView.as_view(), name='task_add'),
    path('task_edit/<int:pk>/', TaskEditView.as_view(), name='task_edit'),
    path('project/', ProjectView.as_view(), name='project'),
    path('project_info/<int:pk>/', ProjectDetailView.as_view(), name='project_info'),
    path('project_add/', ProjectAddView.as_view(), name='project_add'),

    path('users/login/', user_login, name='login'),
    path('user_detail/<int:pk>/', UserDetailView.as_view(), name='user_detail'),
    path('users_all', UsersListView.as_view(), name='users_all'),
    path('add_user/project/<int:pk>/', AddUserProject.as_view(), name='add_user'),
    path('user/<int:pk>/edit', UserChangeView.as_view(), name='edit_profile'),
    path('user/<int:pk>/change_password', ChangePasswordView.as_view(), name='change_password'),

    path('logout/', user_logout, name='logout'),
    path('registration/', UserRegisterView.as_view(), name='registration'),

    path('api/v1/home/', IndexAPIView.as_view()),
    path('api/v1/projects/', ProjectAPIView.as_view()),
    path('api/v1/task_info/<int:pk>/', TaskDetailAPIView.as_view()),
    path('api/v1/project_info/<int:pk>/', ProjectDetailAPIView.as_view()),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)